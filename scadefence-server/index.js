require('module-alias/register');

const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');

const routes = require('./routes');

const { port } = require('./config');

const app = express();

app.use(cors());

app.use(bodyParser.json());
app.use(routes);


app.listen(port, () => {
	console.log('INFO:',`Server successfully started on ${process.env.NODE_ENV} enviroment @ port ${port}`);
});

module.exports = app;
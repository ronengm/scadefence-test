const IPRangesSchema = require('./IPRange.model');

/**
 * Setup the models in the database
 * @param {Mongoose} mongooseConnection
 */
const getModels = (mongooseConnection) => {
	const IPRanges = mongooseConnection.model('IPRange', IPRangesSchema);
	
	return {
		IPRanges
	};

};

module.exports = getModels;


import React from 'react';
import axios from 'axios';

// Config settings
import Config from './Config';

import IPRangesForm from './Containers/Form/IPRangesForm';
import IPRangesList from './Containers/List/IPRangesList';
import './App.css';

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			// Always keeps the full list of ipranges
			ipList: []
		};
	}

	// Load ipranges list from the server
	componentWillMount() {
		axios.get(`${Config.apiURL}/ipranges`)
			.then(res => {
				const ipListData = res.data;
				console.log(ipListData);
				this.setState({ipList: ipListData});
			});
	}

	// Get updated list after adding new iprange
	updateList = (newIPRangesList) => {
		this.setState({ipList: newIPRangesList});
	};


	render() {
		const {ipList} = this.state;
		return (
			<div className="App">
				<IPRangesForm updateList={this.updateList}/>
				<IPRangesList iprangesList={ipList}/>

			</div>
		);
	}
}

export default App;

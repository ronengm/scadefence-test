import React from 'react';
import styled from "styled-components";
import PropTypes from "prop-types";


// IPRange items styling
const IPRangeContainer = styled.div`
	width:100%;
	display: flex;
	flex-direction: row;
	margin-top:1vw;	
`;
const IPRangeBody = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	padding: 0.6rem;
`;


const IPRangeItem = (props) => {
	const ipStartStr = props.iprange.ipStartStr;
	const ipEndStr = props.iprange.ipEndStr;
	const isInternal = props.iprange.isInternal;
	return (
		<IPRangeContainer>
			<IPRangeBody>{ipStartStr} - {ipEndStr}</IPRangeBody>
			{isInternal ?<IPRangeBody>Internal</IPRangeBody>:<IPRangeBody>External</IPRangeBody> }
		</IPRangeContainer>
	);
};


// Set default props
IPRangeItem.defaultProps = {
	iprange: {
		ipStartStr: '1.0.0.1',
		ipEndStr: '1.0.0.1',
		isInternal: false
	}
};

// Set the required props
IPRangeItem.propTypes = {
	iprange: PropTypes.shape({
		ipStartStr: PropTypes.string.isRequired,
		ipEndStr: PropTypes.string.isRequired,
		isInternal: PropTypes.bool.isRequired
	})
};

export default IPRangeItem;